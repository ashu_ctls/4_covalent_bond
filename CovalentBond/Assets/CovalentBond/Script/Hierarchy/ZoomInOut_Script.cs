﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CB = CovalentBond;

namespace CovalentBond
{


	public class ZoomInOut_Script : MonoBehaviour {
		
		public static ZoomInOut_Script instance;
		
		public float rotateSpeed,zoomSpeed;
		public float minZoom, maxZoom;
		public bool isSimulationScene;
		
		internal bool isModlDetected;
		//internal List<GameObject> currModel;
		public GameObject currModel;
		internal GameObject generatorModel;
		// Use this for initialization
		
		#region delegate initialization
		void OnEnable()
		{
			//TrackableEventHandler.onImageTargetDetected += MouthImageDetected;
			LoadModelOnDetection.isModelLoaded += MouthModelLoaded;
		}
		void OnDisable()
		{
			//TrackableEventHandler.onImageTargetDetected -= MouthImageDetected;
			LoadModelOnDetection.isModelLoaded -= MouthModelLoaded;
		}
		#endregion
		
		#region delegate handler
		void MouthModelLoaded(string arg, Transform _transform, bool isFound, LoadModelOnDetection.SceneType sceneType)
		{
			if (isFound)
			{
				currModel = _transform.GetChild(0).gameObject;
			}
			
		}
		#endregion
		
		void Awake () {
			//currModel = new List<GameObject>();
			instance = this;
		}
		
		//Update is called once per frame
		
		void Update()
		{
			//if (isSimulationScene)
			//{
			//    if (Input.touchCount > 0 && Input.GetTouch(0).position.x > Screen.width * 7 / 8) {
			//        return;
			//    }
			//}
			//else {
			//    if ((Input.touchCount > 0 && Input.GetTouch(0).position.x > Screen.width * 3 / 4 /*&& HydroElectric_UIController.instance.isSimulation*/) || !isModlDetected)
			//    {
			//        return;
			//    }
			//}
			
			
			//if (Input.touchCount > 0 && Input.GetTouch(0).position.x > Screen.width * 3 / 4 && HydroElectric_UIController.instance.isSimulation) {
			//    return;
			//}
			
			if (Input.touchCount == 2 )
			{
				Touch touchZero = Input.GetTouch(0);
				Touch touchOne = Input.GetTouch(1);
				
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
				
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
				
				float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
				
				if (isSimulationScene)
				{
					if (/*currModel.Count > 0 &&*/ currModel!=null) {
						currModel.transform.localScale = new Vector3(currModel.transform.localScale.x - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime,
							currModel.transform.localScale.y - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime,
							currModel.transform.localScale.z - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime);
					}
				}
				//else {
				//    for (int i = 0; i < currModel.Count; i++)
				//    {
				//        currModel[i].transform.localScale = new Vector3(currModel[i].transform.localScale.x - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime,
				//       currModel[i].transform.localScale.y - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime,
				//       currModel[i].transform.localScale.z - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime);
				//    }
				//    if (generatorModel != null)
				//    {
				//        generatorModel.transform.localPosition = new Vector3(0, 2.08f * generatorModel.transform.localScale.x, 0);
				//    }
				//}
				
				
				
				
			}
			else if (Input.touchCount == 1 )
			{
				Touch touchZero = Input.GetTouch(0);
				
				if (touchZero.phase == TouchPhase.Moved)
				{
					float swipeSpeed = touchZero.deltaPosition.x;
					
					if (isSimulationScene)
					{
						if (/*currModel.Count > 0 &&*/ currModel != null) {
							currModel.transform.Rotate(new Vector3(0 , 0,-rotateSpeed * swipeSpeed ),Space.Self);
						}
					}
					//else {
					//    for (int i = 0; i < currModel.Count; i++)
					//    {
					//        currModel[i].transform.Rotate(new Vector3(0, -rotateSpeed * swipeSpeed, 0));
					//        Debug.Log("Rotation- " + currModel[i].transform.rotation);
					//    }
					//}
					
				}
				
			}
			
			//for (int i = 0; i < currModel.Count; i++)
			{
				if (currModel!= null)
				{
					if (currModel.transform.localScale.x < minZoom)
					{
						currModel.transform.localScale = new Vector3(minZoom, minZoom, minZoom);
					}
					else if (currModel.transform.localScale.x > maxZoom)
					{
						currModel.transform.localScale = new Vector3(maxZoom, maxZoom, maxZoom);
					}
				}
			}
			
		}
	}
	
}
