﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Vuforia;
using CB = CovalentBond;

namespace CovalentBond
{

	public class DefineAtomNum_Molecules : MonoBehaviour
	{

		public enum molecule{
			ethene, ethane
		}

		public enum NoOfAtom
		{
			Two=2, Three=3, Four=4, Five=5, Six=6 ,Seven=7, Eight=8
		}

		public enum atomType
		{
			same, different
		}

		public static string MoleculeFormation;
		public static int noOfAtom;
		public static int typeOfAtm;
		public static bool isBtnClkd;

		public molecule mol;
		public NoOfAtom numAtom;
		public atomType atmType;
		// Use this for initialization
		public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS { get; private set; }

		void OnEnable()
		{
			LoadModelOnDetection.isModelLoaded+=SECModelLoaded;
		}

		void Start()
		{
			isBtnClkd = false;
		}

		void OnDisable()
		{
			LoadModelOnDetection.isModelLoaded-=SECModelLoaded;
		}

		void SECModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{

			if (status) 
			{

			}
			else 
			{
				//CB.UI_Handler.instance.Description.GetComponentInChildren<Text> ().text = "";
			}
		}

		public void attachMolecule()
		{
			VuforiaUnity.SetHint (HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, (int)transform.GetComponent<DefineAtomNum_Molecules>().numAtom);

			MoleculeFormation = "";
			MoleculeFormation += transform.GetComponent<DefineAtomNum_Molecules> ().mol;
			noOfAtom = (int)transform.GetComponent<DefineAtomNum_Molecules> ().numAtom;
			typeOfAtm = (int)transform.GetComponent<DefineAtomNum_Molecules> ().atmType;

			InitialAct2.DetectionAllowed = true;

			BondFormation.instance.enabled = false;
			BondFormation_for3.instance.enabled = false;
			BondFormation_For2CentralAtom.instance.enabled = true;

			isBtnClkd= true;
//			Debug.Log ();
		}
	}
}




