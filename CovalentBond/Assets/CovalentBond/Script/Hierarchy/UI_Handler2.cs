﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CB = CovalentBond;

namespace CovalentBond 
{
	public class UI_Handler2 : MonoBehaviour
	{
		public static UI_Handler2 instance;

		public GameObject Description;
		public GameObject BeforeAct;
		public GameObject AlertPanel;

		void Start()
		{
			instance = this;
		}
	}
}

