﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CB = CovalentBond;

namespace CovalentBond
{

	public class BondFormation_for3 : MonoBehaviour 
	{


		#region public varaibles.
		public int speed, maxDistance, minDistance;
//		public GameObject scrollViewParent, attachButton, alertPanel, congratsPanel;
//		[SerializeField] List<AtomTransform> atomTransforms = new List<AtomTransform>();
		public List<ImageTargetInfo> detectedObjectList = new List<ImageTargetInfo>();
		public List<MoleculeDescription> moleculeDescription = new List<MoleculeDescription> ();
//		[SerializeField] List<string> attachedObject = new List<string>();
		public static BondFormation_for3 instance;

		#endregion


		#region private variables.
		bool findDistace, modelEnabled;
		float objDistance,objDistance1,objDistance2;
		GameObject centralAtom = null;
		GameObject emptyObject = null;
		static int detectedObject;
		string currentObjectName;

		static int centralIndex, centralIndex2 = -1;
		#endregion
		// Use this for initialization
		void Start () {
			findDistace = false;
			instance.enabled = false;
			modelEnabled = false;
			centralIndex2 = -1;
		}
		
		void OnEnable()
		{
			LoadModelOnDetection.isModelLoaded += HandleOnModelLoaded;
			instance = this;
		}


		private void OnDisable()
		{

			LoadModelOnDetection.isModelLoaded -= HandleOnModelLoaded;

		}

		void Update()
		{
			if (findDistace) 
			{
				//print (objDistance+"..........");
				float step = speed * Time.deltaTime; // calculate distance to move

				for (int i = 0; i < ThreeAtomMolecules.noOfAtom; i++) 
				{
					objDistance=Vector2.Distance(detectedObjectList [i].selectObject.transform.parent.position,detectedObjectList[centralIndex].selectObject.transform.parent.position);
//					if (objDistance > 145) 
//					{
//						CB.UI_Handler2.instance.AlertPanel.GetComponentInChildren<Text> ().text = "Place the central atom in middle";
//						CB.UI_Handler2.instance.AlertPanel.SetActive (true);
//					}
					if (maxDistance > objDistance && objDistance > minDistance) 
					{
						float chkdistance = (150 * ( detectedObjectList [i].selectObject.transform.localScale.x + detectedObjectList [centralIndex].selectObject.transform.localScale.x)/2);
						if (Vector2.Distance(detectedObjectList [i].selectObject.transform.position,detectedObjectList[centralIndex].selectObject.transform.position) > chkdistance) 
						{
							print (".........distance working.......");
							detectedObjectList [i].selectObject.transform.position = Vector3.MoveTowards (detectedObjectList [i].selectObject.transform.position, detectedObjectList [centralIndex].selectObject.transform.position, step);
						} 
						else if (Vector2.Distance (detectedObjectList [i].selectObject.transform.position, detectedObjectList [centralIndex].selectObject.transform.position) <= chkdistance && modelEnabled) 
						{
							modelEnabled = false;
							Debug.LogError ("..........atoms connected.");
							StartCoroutine (DescriptionSet.instance.enableModel (ThreeAtomMolecules.MoleculeFormation.ToLower ()));
						}
					} 
					else 
					{
						detectedObjectList [i].selectObject.transform.localPosition = Vector3.MoveTowards (detectedObjectList [i].selectObject.transform.localPosition, new Vector3(0, 1.2f, 0), step/100);
					}
				}
			}
		}

		private void HandleOnModelLoaded(string cardName, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{
			if (status)
			{
				detectedObject++;
				currentObjectName = cardName;
				string[] initialName = cardName.Split ('_');
//				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower().Contains(TwoAtomMolecules.MoleculeFormation.ToLower()));
				//if (imageTargetInfo == null)
				{
					ImageTargetInfo imageTargetInfo = new ImageTargetInfo();
					imageTargetInfo.cardName = initialName[0];
					imageTargetInfo.imageTarget = _transform.gameObject;
					imageTargetInfo.selectObject = _transform.GetChild(0).gameObject;
					detectedObjectList.Add(imageTargetInfo);
					Debug.Log(detectedObjectList.Count);
					//imageTargetInfo = null;
				}

				centralIndex2 = -1;
				checkCentral ();
				if (CheckList (ThreeAtomMolecules.MoleculeFormation.ToLower ())) 
				{
					print (".......working......");
					findDistace=true;
					modelEnabled = true;

//					StartCoroutine (DescriptionSet.instance.enableModel(ThreeAtomMolecules.MoleculeFormation.ToLower ()));
				} else {
				
					print (".............in else working..........");
					findDistace=false;
				}


			}
			else if (!status)
			{
				StopAllCoroutines ();
				Debug.LogError ("Place correct Card outside........");
				CB.UI_Handler2.instance.AlertPanel.SetActive (false);
				DescriptionSet.instance.DisableModel ();
				currentObjectName = null;
				if (detectedObject > 0)
				{
					detectedObject--;
				}

				string[] initialName = cardName.Split ('_');
				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower() == initialName[0].ToLower());
//				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.selectObject == null);
				if (imageTargetInfo != null)
				{
					detectedObjectList.Remove(imageTargetInfo);
					Debug.Log(detectedObjectList.Count);

				}
				//detectedObjectList.Clear ();
				Debug.Log(detectedObjectList.Count);
			}
		}

		private void checkCentral()
		{
			print ("...................central");
			MoleculeDescription centralMolDes = moleculeDescription.Find (item => item.MoleculeName == ThreeAtomMolecules.MoleculeFormation);
			centralIndex = detectedObjectList.FindIndex(item => item.cardName.ToLower() == centralMolDes.CentralAtom.ToLower());

			for (int i = 0; i < detectedObjectList.Count; i++) 
			{
				if (i != centralIndex && detectedObjectList [i].cardName.ToLower () == centralMolDes.CentralAtom.ToLower ()) {
					centralIndex2 = i;
				} 
//				else 
//				{
//					centralIndex2 = -1;
//				}
			}

			print ("...................checked....central Mol Des......."+centralMolDes.CentralAtom+" "+centralIndex2);
		}

		private bool CheckList (string cmpName)
		{
			int i=0, j=0;

			Debug.LogError ("in checklist..... centralIndex_2 "+centralIndex2);
			if ((detectedObjectList.Count != ThreeAtomMolecules.noOfAtom)||centralIndex2!=-1)
				return false;

			Debug.LogError ("in checklist...>>>.. centralIndex_2 "+centralIndex2);
			for (j = 0; j < detectedObjectList [i].selectObject.GetComponent<ModelDescription> ().CanBeUsed.Count; j++) 
			{
				if (detectedObjectList [i].selectObject.GetComponent<ModelDescription> ().CanBeUsed [j].ToLower () == cmpName) 
				{
					if (i < detectedObjectList.Count-1 ) 
					{
						i++;
					    j = -1;
						continue;
					}


					if (i == detectedObjectList.Count - 1) 
					{
						CB.UI_Handler2.instance.AlertPanel.SetActive (false);
						return true;
					}
				
				}
			}
			CB.UI_Handler2.instance.AlertPanel.GetComponentInChildren<Text> ().text = "Please, place the correct card !";
			CB.UI_Handler2.instance.AlertPanel.SetActive (true);
			return false;
		}

		private void checkPositions()
		{
			for (int i = 0; i < detectedObjectList.Count - 1; i++)
			{
				if (i != centralIndex)
				{
					
				}
			}
		}
	}
}
