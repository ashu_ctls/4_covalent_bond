﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

	public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS {  get; private set; }

	public void gotoAct1()
	{
		SceneManager.LoadScene ("CovalentBond 2");
	}

	public void gotoAct2()
	{
		SceneManager.LoadScene ("CovalentBond 3");
	}

	public void gotoMain()
	{
		SceneManager.LoadScene ("CovalentBond 1");
	}
}
