﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using CB = CovalentBond;

namespace CovalentBond
{
	public class DescriptionSet : MonoBehaviour
	{

		private GameObject currModel;

		public static DescriptionSet instance;

		public List<GameObject> PlanarModels = new List<GameObject>();

		void Awake(){
			instance = this;
//			NumberToSubscript ("Hello23");
//			NumberToSubscript ("He4576llo");
//			NumberToSubscript ("765");
		}

		public void DisableModel()
		{
			for (int i = 0; i < PlanarModels.Count; i++) 
			{
				PlanarModels [i].SetActive (false);
			}
			CB.UI_Handler2.instance.Description.SetActive (false);
		}

		public IEnumerator enableModel(string modelName)
		{
			GameObject model = PlanarModels.Find(item => item.name.ToLower()== modelName.ToLower());
			PrintText (model);
			model.GetComponent<ModelDescription2> ().selected = true;
			yield return new WaitForSeconds (3f);
			//model.SetActive (true);
			currModel= model;
			CB.UI_Handler2.instance.Description.SetActive (true);

			yield return new WaitForSeconds (3f);
			int i = 0;
			do {
				if (PlanarModels [i].GetComponentInChildren<ModelDescription2> ().selected == false) {
					break;
				} else {
					i++;
				}
			} while(i < PlanarModels.Count);
				
			if(i==PlanarModels.Count)
			{
				CB.UI_Handler2.instance.AlertPanel.GetComponentInChildren<Text> ().text = "Conratulations !\n Activity Completed.";
				CB.UI_Handler2.instance.AlertPanel.SetActive (true);

				CB.UI_Handler2.instance.Description.SetActive (false);
				CB.UI_Handler2.instance.BeforeAct.SetActive (false);

				BondFormation.instance.enabled = false;
				BondFormation_for3.instance.enabled = false;
				BondFormation_For2CentralAtom.instance.enabled = false;
			}
			//Debug.LogError (model.GetComponent<ModelDescription2> ().ModelName);
		}

		public void displayStructure()
		{
			currModel.SetActive (true);
		}

		private void PrintText(GameObject t)
		{
			Debug.LogError ("_transform name : "+t.name);
			CB.UI_Handler2.instance.Description.GetComponentInChildren<Text> ().text = "";
			if(t.GetComponentInChildren<ModelDescription2>().ModelName!=null)
				CB.UI_Handler2.instance.Description.GetComponentInChildren<Text>().text += NumberToSubscript(t.GetComponentInChildren<ModelDescription2>().ModelName) +"\n";
			if(t.GetComponentInChildren<ModelDescription2>().State!=null)
				CB.UI_Handler2.instance.Description.GetComponentInChildren<Text>().text += NumberToSubscript(t.GetComponentInChildren<ModelDescription2>().State) +"\n";
			if(t.GetComponentInChildren<ModelDescription2>().BondPair!=null)
				CB.UI_Handler2.instance.Description.GetComponentInChildren<Text>().text += (t.GetComponentInChildren<ModelDescription2>().BondPair) +"\n";
			if(t.GetComponentInChildren<ModelDescription2>().LonePair!=null)
				CB.UI_Handler2.instance.Description.GetComponentInChildren<Text>().text += (t.GetComponentInChildren<ModelDescription2>().LonePair) +"\n";

		}


		#region To find subScript
		string NumberToSubscript(string str)
		{
			string newString = null;

			foreach (char c in str) {
				
				newString += getSubscript(c);
			}

			Debug.LogError (newString);
			return newString;
		}

		string getSubscript(char c)
		{
			switch(c)
			{
			case '0':
				return "\x2080";
			case '1':
				return "\x2081";
			case '2':
				return "\x2082";
			case '3':
				return "\x2083";
			case '4':
				return "\x2084";
			case '5':
				return "\x2085";
			case '6':
				return "\x2086";
			case '7':
				return "\x2087";
			case '8':
				return "\x2088";
			case '9':
				return "\x2089";
			default:
				return c.ToString();
			}
		}

		#endregion
	}

	
}

