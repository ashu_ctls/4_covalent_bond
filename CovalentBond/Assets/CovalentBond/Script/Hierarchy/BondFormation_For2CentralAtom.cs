﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CB = CovalentBond;

namespace CovalentBond
{

	public class BondFormation_For2CentralAtom : MonoBehaviour
	{
		
		#region public varaibles.
		public int speed, maxDistance, minDistance;
		//		public GameObject scrollViewParent, attachButton, alertPanel, congratsPanel;
		//		[SerializeField] List<AtomTransform> atomTransforms = new List<AtomTransform>();
		public List<ImageTargetInfo> detectedObjectList = new List<ImageTargetInfo>();
		public List<ImageTargetInfo> ToMoveObjectList = new List<ImageTargetInfo>();
		public List<MoleculeDescription> moleculeDescription = new List<MoleculeDescription> ();
		//		[SerializeField] List<string> attachedObject = new List<string>();
		public static BondFormation_For2CentralAtom instance;

		#endregion


		#region private variables.
		bool findDistace, modelEnabled;
		float objDistance,objDistance1,objDistance2;
		GameObject centralAtom1 , centralAtom2;
		GameObject emptyObject = null;
		static int detectedObject;
		string currentObjectName;

		static int centralIndex, centralIndex2, centralIndex3 ;
		#endregion

		// Use this for initialization
		void Start () {
			findDistace = false;
			centralAtom1 = centralAtom2 = null;
			centralIndex3 = -1;
			instance.enabled = false;
			modelEnabled = false;
		}

		void OnEnable()
		{
			instance = this;

			LoadModelOnDetection.isModelLoaded += HandleOnModelLoaded;
		}


		private void OnDisable()
		{

			LoadModelOnDetection.isModelLoaded -= HandleOnModelLoaded;

		}

		void Update()
		{
			if (findDistace) 
			{
				//print (objDistance+"..........");
				float step = speed * Time.deltaTime; // calculate distance to move

				for (int i = 0; i < DefineAtomNum_Molecules.noOfAtom; i++) 
				{
					objDistance=Vector2.Distance(detectedObjectList [i].selectObject.transform.parent.position,detectedObjectList[centralIndex].selectObject.transform.parent.position);

					if (maxDistance > objDistance && objDistance > minDistance) {
						float chkdistance = (150 * (detectedObjectList [i].selectObject.transform.localScale.x + detectedObjectList [centralIndex].selectObject.transform.localScale.x) / 2);
						if (Vector2.Distance (detectedObjectList [i].selectObject.transform.position, detectedObjectList [centralIndex].selectObject.transform.position) > chkdistance) {
							print (".........distance working.......");
							detectedObjectList [i].selectObject.transform.position = Vector3.MoveTowards (detectedObjectList [i].selectObject.transform.position, detectedObjectList [centralIndex].selectObject.transform.position, step);
						}
						else if (Vector2.Distance (detectedObjectList [i].selectObject.transform.position, detectedObjectList [centralIndex].selectObject.transform.position) <= chkdistance && modelEnabled) 
						{
							modelEnabled = false;
							Debug.LogError ("..........atoms connected.");
							StartCoroutine (DescriptionSet.instance.enableModel (DefineAtomNum_Molecules.MoleculeFormation.ToLower ()));
						}
					} else if (objDistance > maxDistance+30) 
					{
						//ToMoveObjectList.Add(detectedObjectList[i].)
						if (ToMoveObjectList.Count < 3) 
						{
							ImageTargetInfo imageTargetInfo = new ImageTargetInfo();
							imageTargetInfo.cardName = detectedObjectList[i].cardName;
							imageTargetInfo.imageTarget = detectedObjectList[i].imageTarget;
							imageTargetInfo.selectObject = detectedObjectList[i].selectObject;
							ToMoveObjectList.Add(imageTargetInfo);
						}
					}
					else 
					{
						detectedObjectList [i].selectObject.transform.localPosition = Vector3.MoveTowards (detectedObjectList [i].selectObject.transform.localPosition, new Vector3(0, 1.2f, 0), step/100);
					}
				}

				for (int i = 0; i < ToMoveObjectList.Count; i++) 
				{
					objDistance=Vector2.Distance(ToMoveObjectList [i].selectObject.transform.parent.position,detectedObjectList[centralIndex2].selectObject.transform.parent.position);

					if (maxDistance > objDistance && objDistance > minDistance) 
					{
						float chkdistance = (150 * ( ToMoveObjectList [i].selectObject.transform.localScale.x + detectedObjectList [centralIndex2].selectObject.transform.localScale.x)/2);
						if (Vector2.Distance(ToMoveObjectList [i].selectObject.transform.position,detectedObjectList[centralIndex2].selectObject.transform.position) > chkdistance) 
						{
							print (".........distance working.......");
							ToMoveObjectList [i].selectObject.transform.position = Vector3.MoveTowards (ToMoveObjectList [i].selectObject.transform.position, detectedObjectList [centralIndex2].selectObject.transform.position, step);
						}
					} 
					else 
					{
						ToMoveObjectList [i].selectObject.transform.localPosition = Vector3.MoveTowards (ToMoveObjectList [i].selectObject.transform.localPosition, new Vector3(0, 1.2f, 0), step/100);
					}
				}
			}
		}

		private void HandleOnModelLoaded(string cardName, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{
			if (!status)
			{
				StopAllCoroutines ();

				Debug.LogError ("Place correct Card outside........");
				CB.UI_Handler2.instance.AlertPanel.SetActive (false);
				DescriptionSet.instance.DisableModel ();
				currentObjectName = null;
				if (detectedObject > 0)
				{
					detectedObject--;
				}

				string[] initialName = cardName.Split ('_');
				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower() == initialName[0].ToLower());
//				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.selectObject == _transform.GetChild(0));
				if (imageTargetInfo != null)
				{
					detectedObjectList.Remove(imageTargetInfo);
					Debug.Log(detectedObjectList.Count);

				}


				ToMoveObjectList.Clear ();
				Debug.Log(detectedObjectList.Count);
			}
			else if (status)
			{
				detectedObject++;
				currentObjectName = cardName;
				string[] initialName = cardName.Split ('_');
				//				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower().Contains(TwoAtomMolecules.MoleculeFormation.ToLower()));
				//if (imageTargetInfo == null)
				{
					ImageTargetInfo imageTargetInfo = new ImageTargetInfo();
					imageTargetInfo.cardName = initialName[0];
					imageTargetInfo.imageTarget = _transform.gameObject;
					imageTargetInfo.selectObject = _transform.GetChild(0).gameObject;
					detectedObjectList.Add(imageTargetInfo);
					Debug.Log(detectedObjectList.Count);
					//imageTargetInfo = null;
				}

				centralIndex3 = -1;
				checkCentral ();
				if (CheckList (DefineAtomNum_Molecules.MoleculeFormation.ToLower ())) 
				{
					print (".......working......");
					findDistace=true;
					modelEnabled = true;

//					StartCoroutine (DescriptionSet.instance.enableModel(DefineAtomNum_Molecules.MoleculeFormation.ToLower ()));

				} else {

					print (".............in else working..........");
					findDistace=false;
				}



			}

		}


		private void checkCentral()
		{
			print ("...................central"+DefineAtomNum_Molecules.MoleculeFormation);
			MoleculeDescription centralMolDes = moleculeDescription.Find (item => item.MoleculeName.ToLower() == DefineAtomNum_Molecules.MoleculeFormation);


			centralIndex = detectedObjectList.FindIndex (item => item.cardName.ToLower () == centralMolDes.CentralAtom.ToLower ());


			for (int i = 0; i < detectedObjectList.Count; i++) 
			{
				if (i != centralIndex  && detectedObjectList[i].cardName.ToLower() == centralMolDes.CentralAtom.ToLower ()) {
					centralIndex2 = i;
				}
			}

			for (int i = 0; i < detectedObjectList.Count; i++) 
			{
				if (i != centralIndex  && i != centralIndex2 && detectedObjectList[i].cardName.ToLower() == centralMolDes.CentralAtom.ToLower ()) {
					centralIndex3 = i;
				}
			}
			Debug.Log ("...................checked....central Mol Des......." + centralMolDes.CentralAtom + " " + centralIndex+ " " + centralIndex2);
		}

		private bool CheckList (string cmpName)
		{
			int i=0, j=0;
			if (detectedObjectList.Count != DefineAtomNum_Molecules.noOfAtom || centralIndex3 != -1)
				return false;


			for (j = 0; j < detectedObjectList [i].selectObject.GetComponent<ModelDescription> ().CanBeUsed.Count; j++) 
			{
				if (detectedObjectList [i].selectObject.GetComponent<ModelDescription> ().CanBeUsed [j].ToLower () == cmpName) 
				{
					if (i < detectedObjectList.Count-1 ) 
					{
						i++;
						j = -1;
						continue;
					}

					if (i == detectedObjectList.Count - 1) 
					{
						CB.UI_Handler2.instance.AlertPanel.SetActive (false);
						return true;
					}

				}
			}
			CB.UI_Handler2.instance.AlertPanel.GetComponentInChildren<Text> ().text = "Please, place the correct card !";
			CB.UI_Handler2.instance.AlertPanel.SetActive (true);
			return false;
		}
	}
}





