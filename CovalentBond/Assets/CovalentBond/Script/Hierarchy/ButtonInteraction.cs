﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using CB = CovalentBond;

namespace CovalentBond
{
	public class ButtonInteraction : MonoBehaviour
	{
		static Image selectedImg;
		// Use this for initialization
		public List<Image> CompoundBtns = new List<Image>();

		public static ButtonInteraction instance;

		void Start()
		{
			instance = this;

			if (selectedImg != null)
				selectedImg = null;
			
			for (int i = 0; i < CompoundBtns.Count; i++) 
			{
				CompoundBtns [i].canvasRenderer.SetAlpha(0.3f);
			}
		}

		public void btnClkd(Image self)
		{
			for (int i = 0; i < CompoundBtns.Count; i++) 
			{
				CompoundBtns [i].canvasRenderer.SetAlpha(0.3f);
			}
			selectedImg = self;
			self.canvasRenderer.SetAlpha(1f);
		}

		public void calledInScreenShot ()
		{
			
			for (int i = 0; i < CompoundBtns.Count; i++) 
			{
				CompoundBtns [i].canvasRenderer.SetAlpha(0.3f);
			}

			if(selectedImg != null)
			selectedImg.canvasRenderer.SetAlpha(1f);
		}
	}
}


