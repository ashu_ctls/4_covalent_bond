﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CB = CovalentBond;

namespace CovalentBond
{

	[System.Serializable]
	public class ImageTargetInfo
	{
		public string cardName;
		public GameObject imageTarget;
		public GameObject selectObject;
	}


	[System.Serializable]
	public class AtomTransform
	{
		public string atomName;
		public Transform partsTransform;
	}


	public class BondFormation : MonoBehaviour 
	{


		#region public varaibles.
		public string emptyobjectName = "AssemblePartsParent";
//		public const int maxCardDetection = 2;
		public int speed, maxDistance, minDistance;
//		public GameObject scrollViewParent, attachButton, alertPanel, congratsPanel;
		//[SerializeField] List<AtomTransform> atomTransforms = new List<AtomTransform>();
		public List<ImageTargetInfo> detectedObjectList = new List<ImageTargetInfo>();
		[SerializeField] List<string> attachedObject = new List<string>();
		public static BondFormation instance;

		[Header("Instructions")]
		public string InitialInstruction;
		public string PlaceCorrectCard;
		public string WrongCard;
		public string WrongSeries;
		#endregion


		#region private variables.
		bool findDistace, modelEnabled;
		float objDistance;
		GameObject emptyObject = null;
		static int detectedObject;
		string currentObjectName;
		#endregion
		// Use this for initialization
		void Start () {
			findDistace = false;
			instance.enabled = false;
			modelEnabled = false;
		}
		
		void OnEnable()
		{
			LoadModelOnDetection.isModelLoaded += HandleOnModelLoaded;
			instance = this;
		}

		private void OnDisable()
		{

			LoadModelOnDetection.isModelLoaded -= HandleOnModelLoaded;

		}

		void Update()
		{
			if (findDistace) 
			{
				objDistance=Vector2.Distance(detectedObjectList [0].selectObject.transform.parent.position,detectedObjectList[1].selectObject.transform.parent.position);
				//print (objDistance+"..........");
				float step = speed * Time.deltaTime; // calculate distance to move

				if (maxDistance > objDistance && objDistance > minDistance) {
//					print ("Insisde.........Condition......."+Vector3.Distance(detectedObjectList [0].selectObject.transform.position, detectedObjectList [1].selectObject.transform.position));

					// Check if the positions are approximately close.
					float chkdistance = (160 * ( detectedObjectList [1].selectObject.transform.localScale.x + detectedObjectList [0].selectObject.transform.localScale.x)/2);
					if (Vector2.Distance (detectedObjectList [0].selectObject.transform.position, detectedObjectList [1].selectObject.transform.position) > chkdistance) {
						print (".........distance working.......");
						detectedObjectList [0].selectObject.transform.position = Vector3.MoveTowards (detectedObjectList [0].selectObject.transform.position, detectedObjectList [1].selectObject.transform.position, step);
						detectedObjectList [1].selectObject.transform.position = Vector3.MoveTowards (detectedObjectList [1].selectObject.transform.position, detectedObjectList [0].selectObject.transform.position, step);

					} else if (Vector2.Distance (detectedObjectList [0].selectObject.transform.position, detectedObjectList [1].selectObject.transform.position) <= chkdistance && modelEnabled) 
					{
						modelEnabled = false;
						Debug.LogError ("..........atoms connected.");
						StartCoroutine (DescriptionSet.instance.enableModel (TwoAtomMolecules.MoleculeFormation.ToLower ()));
					}
				} 
				else 
				{
//					detectedObjectList [0].selectObject.transform.position = Vector3.MoveTowards (detectedObjectList [0].selectObject.transform.position, new Vector3(detectedObjectList [0].selectObject.transform.parent.position.x,detectedObjectList [0].selectObject.transform.parent.position.y+1.2f,detectedObjectList [0].selectObject.transform.parent.position.z), step);
//					detectedObjectList [1].selectObject.transform.position = Vector3.MoveTowards (detectedObjectList [1].selectObject.transform.position, new Vector3(detectedObjectList [1].selectObject.transform.parent.position.x,detectedObjectList [1].selectObject.transform.parent.position.y+1.2f,detectedObjectList [1].selectObject.transform.parent.position.z), step);
					detectedObjectList [0].selectObject.transform.localPosition = Vector3.MoveTowards (detectedObjectList [0].selectObject.transform.localPosition, new Vector3(0 , 01.2f,0), step/100);
					detectedObjectList [1].selectObject.transform.localPosition = Vector3.MoveTowards (detectedObjectList [1].selectObject.transform.localPosition, new Vector3(0, 1.2f, 0), step/100);

				}
			}
		}

		private void HandleOnModelLoaded(string cardName, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{
			if (status)
			{
				detectedObject++;
				currentObjectName = cardName;
				string[] initialName = cardName.Split ('_');
//				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower().Contains(TwoAtomMolecules.MoleculeFormation.ToLower()));
				//if (imageTargetInfo == null)
				{
					ImageTargetInfo imageTargetInfo = new ImageTargetInfo();
					imageTargetInfo.cardName = initialName[0];
					imageTargetInfo.imageTarget = _transform.gameObject;
					imageTargetInfo.selectObject = _transform.GetChild(0).gameObject;
					detectedObjectList.Add(imageTargetInfo);
					Debug.Log(detectedObjectList.Count);
					//imageTargetInfo = null;
				}

				if (CheckList (TwoAtomMolecules.MoleculeFormation.ToLower ())) 
				{
					print (".......working......"+TwoAtomMolecules.MoleculeFormation.ToLower ());
					findDistace=true;
					modelEnabled = true;

					//StartCoroutine (DescriptionSet.instance.enableModel(TwoAtomMolecules.MoleculeFormation.ToLower ()));
				} else {
				
					print (".............in else working..........");
					findDistace=false;
				}

			}
			else if (!status)
			{
				StopAllCoroutines ();

				Debug.LogError ("Place correct Card outside........");
				CB.UI_Handler2.instance.AlertPanel.SetActive (false);
				DescriptionSet.instance.DisableModel ();
				currentObjectName = null;
				if (detectedObject > 0)
				{
					detectedObject--;
				}

				string[] initialName = cardName.Split ('_');
				ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower() == initialName[0].ToLower());
				if (imageTargetInfo != null)
				{
					detectedObjectList.Remove(imageTargetInfo);
					Debug.Log(detectedObjectList.Count);
				}

				Debug.Log(detectedObjectList.Count);


			}
		}


		private bool CheckList (string cmpName)
		{
			int i=0, j=0;
			if (detectedObjectList.Count != TwoAtomMolecules.noOfAtom)
				return false;

			if (TwoAtomMolecules.typeOfAtm == 1)
			{
				for (int k = 0; k < detectedObjectList.Count - 1; k++)
					if (detectedObjectList [i].cardName == detectedObjectList [i + 1].cardName)
						return false;
			}

			for (j = 0; j < detectedObjectList [i].selectObject.GetComponent<ModelDescription> ().CanBeUsed.Count; j++) 
			{
				if (detectedObjectList [i].selectObject.GetComponent<ModelDescription> ().CanBeUsed [j].ToLower () == cmpName) {
					if (i < detectedObjectList.Count - 1) {
						i++;
						j = -1;
						continue;
					}


					if (i == detectedObjectList.Count - 1) 
					{
						CB.UI_Handler2.instance.AlertPanel.SetActive (false);
						return true;
					}
				
				} 
			}

			CB.UI_Handler2.instance.AlertPanel.GetComponentInChildren<Text> ().text = "Please, place the correct card !";
			CB.UI_Handler2.instance.AlertPanel.SetActive (true);
			return false;
		}
	}
}
