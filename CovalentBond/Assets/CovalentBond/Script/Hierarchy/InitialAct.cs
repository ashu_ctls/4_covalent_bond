﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using CB = CovalentBond;

namespace CovalentBond
{

	public class InitialAct : MonoBehaviour {

		public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS { get; private set; }
		public int delay;

		void OnEnable()
		{
			LoadModelOnDetection.isModelLoaded+=SECModelLoaded;
		}

		void Start()
		{
			VuforiaUnity.SetHint(HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 1);
			//CB.UI_Handler.instance.Welcome.SetActive (true);
		}

		void OnDisable()
		{
			LoadModelOnDetection.isModelLoaded-=SECModelLoaded;
			//CB.UI_Handler.instance.Welcome.SetActive (false);
		}

		void SECModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{
			CB.UI_Handler.instance.Searching.SetActive (!status);

			StopAllCoroutines ();
			StartCoroutine (selfDelay( status, _transform));
		}

		IEnumerator selfDelay( bool status, Transform _transform)
		{
			yield return new WaitForSeconds (delay);
			CB.UI_Handler.instance.Description.SetActive (status);
			if (status) 
			{
				PrintText (_transform);
			}
			else 
			{
				CB.UI_Handler.instance.Description.GetComponentInChildren<Text> ().text = "";
			}
		}


		void PrintText(Transform t)
		{
			Debug.LogError ("_transform name : "+t.name);
			//CB.UI_Handler.instance.Description.GetComponentInChildren<Text>().text="INFORMATION\n\n";
			CB.UI_Handler.instance.Description.GetComponentInChildren<Text>().text+=t.GetComponentInChildren<ModelDescription>().ModelName +"\n";
			CB.UI_Handler.instance.Description.GetComponentInChildren<Text>().text+=t.GetComponentInChildren<ModelDescription>().symbol +", ";
			CB.UI_Handler.instance.Description.GetComponentInChildren<Text>().text+=t.GetComponentInChildren<ModelDescription>().atomicNo +", ";
			CB.UI_Handler.instance.Description.GetComponentInChildren<Text>().text+=t.GetComponentInChildren<ModelDescription>().MassNo +",\n";
			CB.UI_Handler.instance.Description.GetComponentInChildren<Text>().text+=t.GetComponentInChildren<ModelDescription>().valency +", ";
			CB.UI_Handler.instance.Description.GetComponentInChildren<Text>().text+=t.GetComponentInChildren<ModelDescription>().element +"";

		}
	}
}
