﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using CB = CovalentBond;

namespace CovalentBond
{

	public class InitialAct2 : MonoBehaviour {

		public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS { get; private set; }

		public static bool DetectionAllowed = false;

		void OnEnable()
		{
			LoadModelOnDetection.isModelLoaded+=SECModelLoaded;
		}

		void Start()
		{
			DetectionAllowed = false;
		}

		void OnDisable()
		{
			LoadModelOnDetection.isModelLoaded-=SECModelLoaded;
		}

		void SECModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{
			if((TwoAtomMolecules.isBtnClkd||ThreeAtomMolecules.isBtnClkd||DefineAtomNum_Molecules.isBtnClkd) && status)
			  CB.UI_Handler2.instance.BeforeAct.SetActive (false);
			
//			CB.UI_Handler2.instance.Description.SetActive (status);

			if (status) 
			{
				
				if (!DetectionAllowed)
					_transform.GetChild (0).localScale = Vector3.zero;
			}
			else 
			{
				
			}
		}



	}
}
