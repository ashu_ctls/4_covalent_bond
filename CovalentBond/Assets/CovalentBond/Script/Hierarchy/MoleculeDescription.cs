﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MoleculeDescription 
{

	public string MoleculeName;
	public string CentralAtom;

}

