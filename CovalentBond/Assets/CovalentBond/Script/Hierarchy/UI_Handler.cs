﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CB = CovalentBond;

namespace CovalentBond 
{
	public class UI_Handler : MonoBehaviour
	{
		public static UI_Handler instance;

		public GameObject Description;
		public GameObject Searching;
		//public GameObject Welcome;

		void Start()
		{
			instance = this;
		}
	}
}

