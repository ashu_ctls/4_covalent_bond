﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Vuforia;
using CB = CovalentBond;

namespace CovalentBond
{
	
	public class ThreeAtomMolecules : MonoBehaviour
	{
		public enum molecule{
			carbonDioxide, water, methane, ammonia
		}

		public enum NoOfAtom
		{
			Two=2, Three=3, Four=4, Five=5, Six=6 ,Seven=7, Eight=8
		}

		public enum atomType
		{
			same, different
		}

		public static string MoleculeFormation;
		public static int noOfAtom;
		public static int typeOfAtm;
		public static bool isBtnClkd;

		public molecule mol;
		public NoOfAtom numAtom;
		public atomType atmType;
		// Use this for initialization
		public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS { get; private set; }

		void OnEnable()
		{
			LoadModelOnDetection.isModelLoaded+=SECModelLoaded;
		}

		void Start()
		{
			isBtnClkd = false;
		}

		void OnDisable()
		{
			LoadModelOnDetection.isModelLoaded-=SECModelLoaded;
		}

		void SECModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{
			
			if (status) 
			{

			}
			else 
			{
				//CB.UI_Handler.instance.Description.GetComponentInChildren<Text> ().text = "";
			}
		}
		
		public void attachMolecule()
		{
			VuforiaUnity.SetHint (HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, (int)transform.GetComponent<ThreeAtomMolecules>().numAtom);

			MoleculeFormation = "";
			MoleculeFormation += transform.GetComponent<ThreeAtomMolecules> ().mol;
			noOfAtom = (int)transform.GetComponent<ThreeAtomMolecules> ().numAtom;
			typeOfAtm = (int)transform.GetComponent<ThreeAtomMolecules> ().atmType;

			InitialAct2.DetectionAllowed = true;

			BondFormation.instance.enabled = false;
			BondFormation_for3.instance.enabled = true;
			BondFormation_For2CentralAtom.instance.enabled = false;

			isBtnClkd= true;
//          Debug.Log (".......vuforia hint....."+(int)transform.GetComponent<TwoAtomMolecules>().numAtom+MoleculeFormation);

//			if (transform.GetComponent<TwoAtomMolecules>().mol == (molecule)0) {
//				print ("hydrogen");
//			}
//			else if (transform.GetComponent<TwoAtomMolecules>().mol == (molecule)1) {
//				print ("chlorine");
//			}
//			else if (transform.GetComponent<TwoAtomMolecules>().mol == (molecule)2) {
//				print ("oxygen");
//			}
//			else if (transform.GetComponent<TwoAtomMolecules>().mol == (molecule)3) {
//				print ("Nitrogen");
//			}
		}
	}
}
