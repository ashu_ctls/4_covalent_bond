﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CB = CovalentBond;

namespace CovalentBond
{
	

	public class ModelDescription : MonoBehaviour {

		public string ModelName;
		public string symbol;
		public string atomicNo;
		public string MassNo;
		public string valency;
		public string element;

		public List<string> CanBeUsed;

		// Use this for initialization
		void Start () {
			
		}
		
	
	}
}
