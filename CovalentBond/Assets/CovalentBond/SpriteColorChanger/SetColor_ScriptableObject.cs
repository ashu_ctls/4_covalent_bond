﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UIColorSetting", menuName = "WizAr/Create UiColorSetting", order = 1)]
public class SetColor_ScriptableObject : ScriptableObject {

	public Color uiColor,uiTextColor;
}
