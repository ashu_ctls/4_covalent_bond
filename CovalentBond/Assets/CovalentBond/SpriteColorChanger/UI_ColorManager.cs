﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ColorManager : MonoBehaviour {

	public SetColor_ScriptableObject uiColorSetting;

	[Header("UI panels and btns")]
	public List<Image> uiImages;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < uiImages.Count; i++) {
			uiImages [i].color = uiColorSetting.uiColor;
			if (uiImages [i].GetComponentInChildren<Text> () != null) {
				uiImages [i].GetComponentInChildren<Text> ().color = uiColorSetting.uiTextColor;
			}
		}
	}

}
