﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScreenShotData
{
	private  string name;
	private  Texture2D texture;

	public string Name {
		set{ name = value; }
		get{ return name; }
	}

	public Texture2D texture2D {
		set{ texture = value; }
		get{ return texture; }
	}
}